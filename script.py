#!/usr/bin/python3

import time
import zmq


def receive_msq():
    context = zmq.Context()
    receiver = context.socket(zmq.PULL)
    receiver.connect("tcp://api:5557")

    while True:
        msg = receiver.recv_json()
        db_entry_id = msg['id']
        print('here Im quering the db for entry with id: %s' % db_entry_id)
        time.sleep(10)


receive_msq()
