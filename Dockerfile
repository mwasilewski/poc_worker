FROM python:3
RUN pip install zmq
ADD script.py /
CMD [ "python", "-u", "./script.py" ]
