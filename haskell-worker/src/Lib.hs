module Lib
    ( someFunc
    ) where

someFunc :: IO ()
someFunc = do
  putStrLn "taking a message from the 0mq queue"
  putStrLn "quering the db using id from 0mq msg"
  putStrLn "processing the data"
  putStrLn "uploading processed data to db"
