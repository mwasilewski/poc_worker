import zmq
import time
import random

QUEUE_HOST = 'queue'
QUEUE_HOST = '127.0.0.1'
QUEUE_PORT = 7000


def job_handle(job):
    print("Taken care of: {}.".format(job))
    time.sleep(10)


def receive_msq():
    worker_id = random.randint(1, 200)
    print("Started worker: {}. Connecting to: {}:{}".format(
        worker_id, QUEUE_HOST, QUEUE_PORT)
    )
    context = zmq.Context()
    receiver = context.socket(zmq.PULL)
    receiver.connect("tcp://{}:{}".format(QUEUE_HOST, QUEUE_PORT))

    while True:
        msg = receiver.recv_json()
        db_entry_id = msg['id']
        print("Worker {}) OK received: {}".format(worker_id, db_entry_id))
        job_handle(msg)
        response_port = msg['port']

        confirmation_sender = context.socket(zmq.PUSH)
        confirmation_sender.connect("tcp://{}:{}".format(
            QUEUE_HOST, response_port)
        )
        response = {"status": "OK"}
        confirmation_sender.send_json(response)
        confirmation_sender.close()


if __name__ == "__main__":
    receive_msq()
